use axum::{
    routing::{get, post},
    Form, Router,
};
use tokio::net::TcpListener;

#[derive(serde::Deserialize)]
struct FormData {
    name: String,
    email: String,
}

pub async fn run(listener: TcpListener) -> Result<(), std::io::Error> {
    // build our application with routes
    let app = Router::new()
        .route("/health_check", get(health_check))
        .route("/subscriptions", post(subscribe));
    // run our app with hyper, listening through a given listener
    //let listener = tokio::net::TcpListener::bind(address).await.unwrap();
    axum::serve(listener, app).await
}

async fn health_check() {}

async fn subscribe(_form: Form<FormData>) {}
